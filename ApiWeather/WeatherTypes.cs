﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWeather
{
    public enum WeatherTypes
    {
        Sunny,
        PartlyCloudy,
        Cloudy,
        Rainy,
        PouringRain

    }
}
