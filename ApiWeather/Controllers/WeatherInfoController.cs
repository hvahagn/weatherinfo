﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiWeather.Models;
using System.Linq;

namespace ApiWeather.Controllers
{
    [Route("api/[controller]")]
    public class WeatherInfoController : Controller
    {
        private static List<WeatherInfo> weatherInfoList = new List<WeatherInfo>
        {
            new WeatherInfo()
            {
                Id=1,
                City="Yerevan",
                Date=DateTime.Today,
                MaxTemperature=35,
                MinTemperature=-10,
                WeatherType=WeatherTypes.Sunny

            },
             new WeatherInfo()
            {
                 Id=2,
                City="Moscow",
                Date=DateTime.Today,
                MaxTemperature=50,
                MinTemperature=-40,
                WeatherType=WeatherTypes.Sunny

            }
        };
        // GET api/values
        [HttpGet]
        public IEnumerable<WeatherInfo> Get()
        {
            return weatherInfoList;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public WeatherInfo Get(int id)
        {
            return weatherInfoList.FirstOrDefault(wi=>wi.Id==id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
